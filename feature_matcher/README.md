# Product Features Matching
This module enables key-value feature clustering and matching using state-of-the art embedding dimensionality reduction and clustering algorithms.

For example, in the case where we have several descriptions of a product coming from different sources as given below, we want to match features that describe the same characteristics.
```json
{
    "weight": "10 kg",
    "height": "100 cm",
    "depth": "40 cm",
    "width": "30 cm",
    "energy consumption": "3255 kWh/year",
}
{
    "net weight": "10 kg",
    "product dimensions": "30 x 100 x 40 cm",
    "annual consumption (kWh)": "3255",
}
```

The difficulties in the task is that fields are named differently, and sometimes there is not a one to one match of characteristics.
Ideally we would like to have as an output something that looks like the following:

```yaml
clusters:
  - name: weight
    clustered:
      - weight
      - net weight
  - name: dimensions
    clustered:
      - height
      - width
      - depth
      - product dimensions
  - name: energy_consumtion
    clustered:
      - energy consumption
      - annual consumption (kWh)
```

This is what this module is aimed at.

# How clustering is done
The clustering consists of two main steps:
- Embed: assign a vector to each key-value pair
- Cluster: regroup vectors by how similar they are in the embedding vector space.

Additionnaly, to assist and incrementally improve the clustering, we've included a semi-supervised version, for which a semi-supervised dimensionality reduction step is added, which is optimised to pack together labels that are assigned a target cluster. Since this process is not 100% accurate, a correction step is added after the clustering step to make sure we don't degrade an existing cluster assignment.
This way the feature_matcher is an incremental process with a human-in-the-loop.

# Pre-requisites

You need to have docker installed

# Usage

## Download docker image (1st use or to upgrade version)
```bash
docker pull ycouble/nvc_cli
```
This may take a while the first time (need to download all layers, around 2GB to download)

## Get help
```bash
docker run ycouble/nvc_cli  --help
```

## Main commands
**Warning**: All files referenced in the commands **MUST** be accessible from the directory where you run the command.

e.g. if you are located in `/home/my_user/dev`, the validated feature files and the catalog file must be in `/home/my_user/dev/` or any of its sub-directories.

### Run clustering for a category
Synopsis:
```bash
docker run -v $(pwd):/app_data ycouble/nvc_cli category CATALOG_FILE CATEGORY_IDS [--validated_features_file=VALIDATED_FEATURES_FILE] [--output_file=OUTPUT_FILE]
```

- CATALOG_FILE: relative path to the CSV file containing all the product descriptions as well as the product type information. For example:
```csv
product_id,data_icecat,data_boulanger,product_type_id,product_type_name
9439,,"{""Garantie"": ""2 ans"", ...}",,,,,,737,Four électrique encastrable
```
- CATEGORY_IDS: one or several product_type_id to include in the clustering
- VALIDATED_FEATURES_FILE (Optional): relative path to the YML file containing the validated features. If not provided, clustering is run from scratch. Useful for a first iteration on a completely new category.
- OUTPUT_FILE (Optional): desired output filename. Defaults to clusters_{date}.yml

### Run globally
This command may be useful once many category runs have been performed. It runs the clustering for the entire catalog. Syntax is very similar

Synopsis:
```bash
docker run -v $(pwd):/app_data ycouble/nvc_cli global CATALOG_FILE [--validated_features_file=VALIDATED_FEATURES_FILE] [--output_file=OUTPUT_FILE]
```

### Feature file checking
#### Format YML specification
Feature files must respect the folowing conventions
```yml
clusters:  # MANDATORY keyword
  # The for each cluster
  - name: NAME OF THE CLUSTER  # MANDATORY
    validated:  # MANDATORY
      - key 1
      - key 2
      - ...
    clustered:  # OPTIONAL: Ignored if the file is used as a validated feature file
      - key 3
    potential:  # OPTIONAL: Ignored if the file is used as a validated feature file
      - key 4
```
- Keys are present in **one and only one** cluster and section (except if duplicated keys are provided in a validated feature file)
- cluster names do not need to be unique (although it is probably not recommended)
- This format is exactly what is provided as output by the program

#### `brief` Command
Command that outputs a small summary of the feature file (number of clusters, keys etc.)
Synopsis:
```bash
docker run -v $(pwd):/app_data ycouble/nvc_cli validation FILE_TO_CHECK
```

#### `validation` Command
Synopsis:
```bash
docker run -v $(pwd):/app_data ycouble/nvc_cli validation FILE_TO_CHECK [VALIDATED_FEATURES_FILE]
```
- FILE_TO_CHECK: path to the feature file to check
- VALIDATED_FEATURES_FILE (Optional): path to the validated feature to use to check validated features conservation
