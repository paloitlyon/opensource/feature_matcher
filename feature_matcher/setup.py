import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="feature-matcher",
    version="1.0.0a1",
    author="Yoann Couble",
    author_email="ycouble@palo-it.com",
    description="A product feature matcher using sentence-transformers",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=["feature_matcher"],
    install_requires=[
        "numpy",
        "pandas",
        "matplotlib",
        "seaborn",
        "hvplot",
        "holoviews",
        "pyyaml",
        "hdbscan",
        "umap-learn",
        "sklearn",
        "spacy",
        "sentence-transformers",
        "textacy",
        "fire",
    ],
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Operating System :: POSIX :: Linux",
        "Development Status :: 1 - Planning",
        "Intended Audience :: Science/Research",
    ],
)
