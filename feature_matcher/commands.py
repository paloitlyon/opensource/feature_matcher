import logging
import sys
from datetime import datetime
from typing import List, Union

import fire

from feature_matcher.entities import Catalog

PATH_PREFIX = "/app_data"

logger = logging.getLogger("main")
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(levelname)8s - %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)


def run(cat: Catalog, output_file, validated_features_file):
    from feature_matcher.clustering.clusterer import (
        Clusterer,
        cluster_umap_hdbscan,
        embed_sentence_transformers,
    )
    from feature_matcher.clustering.pre_clusterer import PreClusterer

    if output_file is None:
        output_file = f"clusters_{datetime.now().strftime('%Y%m%d%H%M%S')}.yml"
    logger.info(cat.brief())
    if validated_features_file:
        pre_clusterer = PreClusterer.from_yml(
            PATH_PREFIX + "/" + validated_features_file
        )
        logger.info(pre_clusterer.brief())
        cat = pre_clusterer.augment_catalog(cat)
        clusterer = Clusterer(
            embed_func=embed_sentence_transformers,
            embed_kwargs=dict(show_progress_bar=True),
            cluster_func=cluster_umap_hdbscan,
            pre_clusterer=pre_clusterer,
        )
    else:
        clusterer = Clusterer(
            embed_func=embed_sentence_transformers, cluster_func=cluster_umap_hdbscan
        )
    logger.info("Vectorizing...")
    cat_features, _ = cat.get_features_and_labels()
    clusterer.embed(cat_features)
    logger.info("Clustering...")
    clusterer.cluster()
    clustering = clusterer.to_clustering()
    logger.info("Formatting output...")
    clustering = clustering.fix_preclustered()
    clustering.to_yml(PATH_PREFIX + "/" + output_file)


def run_global(
    catalog_file: str, validated_features_file: str = None, output_file: str = None
):
    logger.info("Reading data...")
    cat = Catalog.global_from_csv(PATH_PREFIX + "/" + catalog_file)
    run(cat, output_file, validated_features_file)


def run_category(
    catalog_file: str,
    category_ids: Union[List[int], int],
    validated_features_file: str = None,
    output_file: str = None,
):
    """
    Runs the clustering for a list of categories, identified by their ids
    If a validated feature file is provided, it is used in the clustering
    and they will be kept in the output
    """
    if isinstance(category_ids, int):
        category_ids = [category_ids]
    logger.info("Reading data...")
    cat = Catalog.global_from_csv(
        PATH_PREFIX + "/" + catalog_file, category_ids=category_ids
    )
    run(cat, output_file, validated_features_file)


def validation(
    file_to_validate: str,
    feature_validation_file: str = None,
    list_clusters: bool = False,
):
    """
    Checks if there are any error in the validated feature file:
      - missing "validated" field in a cluster
      - readability issues
      - duplicate key
      - duplicate cluster/feature name
      - cluster spreading (feature that chaged cluster)
      - feature lost
    Also optionally list the cluster/feature names in alphabetical order
    """
    import yaml

    brief(file_to_validate)

    file_to_validate = PATH_PREFIX + "/" + file_to_validate
    if feature_validation_file:
        feature_validation_file = PATH_PREFIX + "/" + feature_validation_file
    with open(file_to_validate, "r") as f:
        clustering = yaml.load(f, Loader=yaml.CLoader)["clusters"]

    # Missing validated
    error = False
    logger.info("# Validation")
    logger.info("## Checking formatting...")
    for i, cluster in enumerate(clustering):
        if "name" not in cluster:
            error = True
            logger.error(f"  - Missing name for cluster {i}")
        if "validated" not in cluster:
            error = True
            logger.error(
                f"  - Missing validated field for cluster {cluster.get('name', i)}"
            )
    if error:
        logger.error("Errors detected, fix them before re-running the validation")
        return
    else:
        logger.info("Formatting OK")

    # Duplicate key
    errors = 0
    key_to_cluster_names = {}
    logger.info("## Checking duplicates...")
    for cluster in clustering:
        for category in ["validated", "clustered", "potential"]:
            for key in cluster[category]:
                if key in key_to_cluster_names:
                    logger.error(
                        f"  - key {key} seen twice in file, in clusters "
                        f"{key_to_cluster_names[key]} and {cluster['name']} (section '{category}')"
                    )
                    errors += 1
                    continue
                key_to_cluster_names[key] = cluster["name"]
    if errors:
        logger.error(f"{errors} duplicates, please fix")
    else:
        logger.info("Duplicates OK")

    # Duplicate cluster name
    name_errors = 0
    names = []
    logger.info("## Checking duplicate names...")
    for cluster in clustering:
        if cluster["name"] in names:
            logger.error(f"  - cluster name {cluster['name']} appears several times")
            name_errors += 1

    if name_errors:
        logger.error(f"{name_errors} duplicated names, please fix")
    else:
        logger.info("Name duplicates OK")

    if feature_validation_file:
        with open(feature_validation_file, "r") as f:
            validated = yaml.load(f, Loader=yaml.CLoader)["clusters"]

        # Cluster spread
        logger.info("## Cluster spreading validation")
        logger.warning("Not implemented")

        # Feature lost
        valid_errors = 0
        logger.info("## Checking validated keys conservation")
        for cluster in validated:
            for key in cluster["validated"]:
                if key not in key_to_cluster_names:
                    logger.error(
                        f"  - Validated key {key} (cluster {cluster['name']}) "
                        f"is missing in the candidate file"
                    )
                    errors += 1
        if valid_errors:
            logger.error(
                f"{valid_errors} validated features not kept in candidate file"
            )
        else:
            logger.info("Validated feature conservation OK")

    if list_clusters:
        logger.info("# Cluster list")
        clist = "\n".join(sorted([cluster["name"] for cluster in clustering]))
        logger.info(f"\n{clist}")


def brief(feature_validation_file: str):
    """
    Prints a brief of the give feature validation file
    - nb clusters, nb new clusters (no validated features)
    - min/max cluster size
    - ...
    """
    import yaml

    feature_validation_file = PATH_PREFIX + "/" + feature_validation_file

    sections = ["validated", "clustered", "potential"]
    with open(feature_validation_file, "r") as f:
        validated = yaml.load(f, Loader=yaml.CLoader)["clusters"]
    names = [cluster["name"] for cluster in validated]
    new = [cluster for cluster in validated if len(cluster.get("validated", [])) == 0]
    unchanged = [
        cluster["name"]
        for cluster in validated
        if (len(cluster.get("clustered", [])) == 0)
        and (len(cluster.get("potential", [])) == 0)
    ]
    sizes = [{cat: len(cluster[cat]) for cat in sections} for cluster in validated]
    total_sizes = [sum(cluster.values()) for cluster in sizes]
    summary_sizes = {cat: sum(cluster[cat] for cluster in sizes) for cat in sections}
    min_size = min(total_sizes)
    max_size = max(total_sizes)
    logger.info(f"# Summary")
    logger.info("## Clusters")
    logger.info(f"- {len(names)} Clusters")
    logger.info(f"- {len(new)} New clusters")
    logger.info(f"- {len(unchanged)} Unchanged ({', '.join(unchanged)})")
    logger.info("## Features")
    logger.info(f"- {sum(total_sizes)} features in total")
    logger.info(f"  - {summary_sizes['validated']} validated features")
    logger.info(f"  - {summary_sizes['clustered']} clustered features")
    logger.info(f"  - {summary_sizes['potential']} potential features")
    logger.info(f"- min cluster size = {min_size}")
    logger.info(f"- max cluster size = {max_size}")


if __name__ == "__main__":
    fire.Fire(
        {
            "global": run_global,
            "category": run_category,
            "validation": validation,
            "brief": brief,
        }
    )
