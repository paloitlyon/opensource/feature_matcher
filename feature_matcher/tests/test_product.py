from feature_matcher.entities import Feature, Match, MatchList, Product


def test_match_products():
    p1 = Product({"a": "vala"})
    p2 = Product({"a": "valb"})
    matches = p1.match(p2)
    assert len(matches) == 1
    m = matches[0]
    assert m.source == Feature("a", "vala").name
    assert m.target == Feature("a", "valb").name
    assert m.score == 1


def test_match_prod_custom_sim():
    def sim(f1, f2):
        return 5

    p1 = Product({"a": "vala"})
    p2 = Product({"a": "valb"})
    matches = p1.match(p2, sim_func=sim)
    assert len(matches) == 1
    m = matches[0]
    assert m.score == 5


def test_match_prod_custom_match():
    def match(p1, p2, sim_func=None):
        return MatchList([])

    p1 = Product({"a": "vala"})
    p2 = Product({"a": "valb"})
    matches = p1.match(p2, match_func=match)
    assert len(matches) == 0
