from feature_matcher.entities import Feature


def test_equality():
    assert Feature("test", "value") == Feature("test", "value")


def test_eq_diff():
    assert Feature("test", "value") != Feature("test", "other value")
