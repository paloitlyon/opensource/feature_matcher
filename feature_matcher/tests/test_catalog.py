from feature_matcher.entities import Catalog, Product


def test_init_product():
    p1 = Product({"a": "vala", "b": "valb"})
    p2 = Product({"c": "valc", "d": "vald"})
    c = Catalog("test", [p1, p2])
    assert len(c.products) == 2
    assert len(c.aggregated) == 4


def test_init_dict():
    p1 = {"a": "vala", "b": "valb"}
    p2 = {"c": "valc", "d": "vald"}
    c = Catalog("test", [p1, p2])
    assert len(c.products) == 2
    assert len(c.aggregated) == 4


def test_aggregation():
    p1 = {"a": "vala", "b": "valb"}
    p2 = {"a": "vala2", "d": "vald"}
    c = Catalog("test", [p1, p2])

    assert len(c.aggregated) == 3
    assert len(c.aggregated["a"]) == 2


def test_match():
    p1 = {"a": "vala", "b": "valb"}
    p2 = {"c": "valc", "d": "vald"}
    c = Catalog("test", [p1, p2])
    c2 = Catalog("test", [p1])

    matches = c.matches(c2)
    assert len(matches) == 8
    a_to_a = [m for m in matches if m.source == "a" and m.target == "a"][0]
    assert a_to_a.score == 1
