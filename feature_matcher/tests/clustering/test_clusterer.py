import pytest

from feature_matcher.clustering.clusterer import (
    Clusterer,
    cluster_hdbscan,
    cluster_umap_hdbscan,
    embed_sentence_transformers,
    embed_words_spacy,
)

SAMPLE_TEXTS = ["Poids : 10kg", "Capacité: 10L", "Commentaires : chauffe trop"]


@pytest.mark.slow
@pytest.mark.parametrize("embed_func", [embed_words_spacy, embed_sentence_transformers])
def test_embed(embed_func):
    embeddings = embed_func(SAMPLE_TEXTS)
    assert len(embeddings) == len(SAMPLE_TEXTS)


@pytest.mark.slow
@pytest.mark.parametrize("cluster", [cluster_hdbscan, cluster_umap_hdbscan])
def test_cluster(cluster):
    vectors = embed_sentence_transformers(SAMPLE_TEXTS * 100)
    cluster(vectors)


@pytest.mark.parametrize("embed", [embed_sentence_transformers, embed_words_spacy])
@pytest.mark.parametrize("cluster", [cluster_hdbscan, cluster_umap_hdbscan])
def test_init_clusterer(embed, cluster):
    c = Clusterer(embed_func=embed, cluster_func=cluster)
