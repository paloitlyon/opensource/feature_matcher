from feature_matcher.entities import Matching, Product

gt = {
    "Type de commande": "Type de commande",
    "Capacité nette du four": "Volume en Litres",
    "Puissance Totale du Four": "Puissance totale",
    "Classe d'efficacité énergétique": "Classe énergétique",
    "Porte à fermeture à amortissement": "Fermeture de porte",
}

ref = {
    "Type de commande": "Tactile",
    "Nombre de clayettes": "5 étagères",
    "Capacité nette du four": "73 L",
    "Puissance Totale du Four": "3650 W",
    "Emplacement charnière de porte": "Bas",
    "Classe d'efficacité énergétique": "A+",
    "Plage d’efficacité énergétique": "A+++ à D",
    "Porte à fermeture à amortissement": "Oui",
}

other = {
    "Puissance totale": "3.650 W",
    "Type de commande": "Tactile",
    "Volume en Litres": "73 L",
    "Fermeture de porte": "Fermeture douce",
    "Type d'encastrement": "Four standard (60 x 60 cm)",
    "Classe énergétique": "A+",
    "Vitre de porte démontable": "Oui, pour un nettoyage complet et facilité",
}


def test_from_dict():
    gt_matching = Matching.from_dict(gt)
    assert len(gt_matching) == 5
    assert gt_matching[0].score == 1


def test_nonreg_matching():
    pref = Product(ref)
    pother = Product(other)
    gt_matching = Matching.from_dict(gt)
    matching = pref.match(pother)
    assert len(matching) == 1
    assert list(matching.dict.keys())[0] in gt.keys()
    assert matching.get_score(gt_matching) == 0.2
