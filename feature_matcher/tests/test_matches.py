from feature_matcher.entities import Feature, Match, MatchList


def test_eq():
    assert Match(Feature("a", "b"), Feature("c", "d"), 0.5) == Match(
        Feature("e", "f"), Feature("g", "h"), 0.5
    )


def test_neq():
    assert Match(Feature("a", "b"), Feature("c", "d"), 0.5) != Match(
        Feature("a", "b"), Feature("c", "d"), 1
    )


def test_comparison():
    assert Match(Feature("a", "b"), Feature("c", "d"), 0.5) >= Match(
        Feature("e", "f"), Feature("g", "h"), 0.5
    )


def test_comparison_2():
    assert Match(Feature("a", "b"), Feature("c", "d"), 0.5) > Match(
        Feature("e", "f"), Feature("g", "h"), 0.1
    )


def test_comparison_3():
    assert not (
        Match(Feature("a", "b"), Feature("c", "d"), 0.5)
        > Match(Feature("e", "f"), Feature("g", "h"), 0.9)
    )


def test_best():
    a = Feature("a", "a")
    b = Feature("b", "b")
    ml = MatchList([Match(a, b, i) for i in range(10)])
    assert ml.get_best().score == 9


def test_remove_feature():
    a = Feature("a", "a")
    abis = Feature("a", "abis")
    b = Feature("b", "b")
    c = Feature("c", "c")
    ml = MatchList(
        [
            Match(a, b, 1),
            Match(a, c, 1),
            Match(abis, c, 1),
            Match(b, a, 1),
        ]
    )
    ml.remove_feature(a)
    assert len(ml) == 2


def test_add():
    a = Feature("a", "a")
    abis = Feature("a", "abis")
    b = Feature("b", "b")
    c = Feature("c", "c")
    ml = MatchList(
        [
            Match(a, b, 1),
            Match(a, c, 1),
        ]
    )
    ml2 = MatchList(
        [
            Match(abis, c, 1),
            Match(b, a, 1),
        ]
    )
    assert len(ml + ml2) == 4


def test_remove_from_match():
    a = Feature("a", "a")
    abis = Feature("a", "abis")
    b = Feature("b", "b")
    c = Feature("c", "c")
    ml = MatchList(
        [
            Match(a, b, 1),
            Match(a, c, 1),
            Match(abis, c, 1),
            Match(b, a, 1),
        ]
    )
    ml.remove_from_match(Match(a, c, 1))
    assert len(ml) == 1
