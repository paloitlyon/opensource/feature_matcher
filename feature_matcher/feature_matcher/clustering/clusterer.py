from typing import List, Optional

import hdbscan
import numpy as np
import spacy
import umap
from sentence_transformers import SentenceTransformer
from torch import embedding
from tqdm import tqdm

from feature_matcher.clustering.clustering import Clustering
from feature_matcher.clustering.pre_clusterer import PreClusterer
from feature_matcher.entities import Feature


class Clusterer:
    def __init__(
        self,
        embed_func,
        cluster_func,
        embed_kwargs: Optional[dict] = None,
        cluster_kwargs: Optional[dict] = None,
        pre_clusterer: PreClusterer = None,
        features: List[Feature] = None,
        labels: List[str] = None,
        embeddings=None,
    ):
        self.embed_func = embed_func
        self.embed_kwargs = embed_kwargs if embed_kwargs else {}

        self.cluster_func = cluster_func
        self.cluster_kwargs = cluster_kwargs if cluster_kwargs else {}

        self.pre_clusterer = pre_clusterer
        self.semi_supervised = pre_clusterer is not None

        self.clusterer = None
        self.soft_clusters = None

        self.features = features
        self.labels = labels
        self.embeddings = embeddings
        if self.embeddings is not None and (
            self.features is None or self.labels is None
        ):
            raise ValueError("Embeddings, labels and features must be set together")

    def embed(self, features: List[Feature] = None, force_recompute=False):
        if self.embeddings is not None and not force_recompute:
            print(
                "Embeddings already computed, skipping (use force_recompute=True to force it)."
            )
            return self
        elif self.embeddings is None and features is not None:
            texts = [f"{feature.name} : {feature.value}" for feature in features]
            self.features = features
            self.labels = texts
        elif self.embeddings is None and features is None:
            raise ValueError(
                "Either provide features in the call or pre-load the embeddings"
            )
        self.embeddings = self.embed_func(self.labels, **self.embed_kwargs)
        return self

    def cluster(self):
        targets = None
        if self.semi_supervised:
            targets = self.pre_clusterer.fit_transform(self.features)
        self.clusterer, self.soft_clusters = self.cluster_func(
            self.embeddings, targets=targets, **self.cluster_kwargs
        )
        return self

    def to_clustering(self):
        if self.clusterer is None:
            raise AssertionError(
                "Must compute clusters before transforming to a clustering"
            )
        return Clustering(
            self.features,
            self.labels,
            self.clusterer.labels_,
            soft_clusters=self.soft_clusters,
            pre_clusterer=self.pre_clusterer,
        )


class NLP:
    """Singleton to avoid loading too many times the model"""

    __instance = {}

    def __new__(cls, model_name="fr_core_news_lg", *args, **kwargs):
        if cls.__instance.get(model_name) is None:
            cls.__instance[model_name] = spacy.load(model_name)
        return cls.__instance[model_name]


class SentenceNLP:
    """Singleton to avoid loading too many times the model"""

    __instance = {}

    def __new__(
        cls, model_name="inokufu/flaubert-base-uncased-xnli-sts", *args, **kwargs
    ):
        if cls.__instance.get(model_name) is None:
            cls.__instance[model_name] = SentenceTransformer(model_name)
        return cls.__instance[model_name]


################# Embed functions


def embed_words_spacy(
    texts: List[str],
    model_name="fr_core_news_lg",
    show_progress_bar=False,
    n_process=8,
    **kwargs,
):
    nlp = NLP(
        model_name,
        exclude=[
            "morphologizer",
            "tagger",
            "parser",
            "senter",
            "attribute_ruler",
            "lemmatizer",
            "ner",
        ],
    )
    doc_generator = nlp.pipe(texts, n_process=n_process, **kwargs)
    if show_progress_bar:
        return np.array([doc.vector for doc in tqdm(doc_generator, total=len(texts))])
    return np.array([doc.vector for doc in doc_generator])


def embed_sentence_transformers(
    texts: List[str],
    model_name="inokufu/flaubert-base-uncased-xnli-sts",
    show_progress_bar=False,
    **kwargs,
):
    model = SentenceNLP(model_name)
    return model.encode(texts, show_progress_bar=show_progress_bar, **kwargs)


################# Reduce functions


def reduce_dims_umap(embeddings, targets=None, target_dim=50):
    if targets:
        clusterable_embeddings = umap.UMAP(
            n_neighbors=30,
            min_dist=0.0,
            n_components=target_dim,
            random_state=42,
        ).fit_transform(embeddings, targets)
    else:
        clusterable_embeddings = umap.UMAP(
            n_neighbors=30,
            min_dist=0.0,
            n_components=target_dim,
            random_state=42,
        ).fit_transform(embeddings)
    return clusterable_embeddings


################# Cluster functions


def cluster_hdbscan(
    embeddings, min_cluster_size=2, min_samples=2, compute_soft_clusters=True
):
    clusterer = hdbscan.HDBSCAN(
        min_cluster_size=min_cluster_size,
        min_samples=min_samples,
        prediction_data=compute_soft_clusters,
    )
    clusterer.fit(embeddings)
    nb_clusters = clusterer.labels_.max()
    print(
        f"Clustering done, created {nb_clusters} clusters, {sum(clusterer.labels_ == -1)} unclustered data points"
    )
    if compute_soft_clusters:
        soft_clusters = hdbscan.all_points_membership_vectors(clusterer)
    else:
        soft_clusters = None
    return clusterer, soft_clusters


def cluster_umap_hdbscan(
    embeddings,
    targets=None,
    hdbscan_min_cluster_size=2,
    hdbscan_min_samples=2,
    umap_target_dim=50,
    hdbscan_compute_soft_clusters=True,
):
    clusterable_embeddings = reduce_dims_umap(
        embeddings, targets=targets, target_dim=umap_target_dim
    )
    return cluster_hdbscan(
        clusterable_embeddings,
        min_cluster_size=hdbscan_min_cluster_size,
        min_samples=hdbscan_min_samples,
        compute_soft_clusters=hdbscan_compute_soft_clusters,
    )
