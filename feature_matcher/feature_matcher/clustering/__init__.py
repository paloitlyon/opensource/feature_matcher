from typing import List

import hdbscan
import hvplot.pandas  # noqa
import numpy as np
import spacy
from sentence_transformers import SentenceTransformer

from feature_matcher.entities import Catalog

from .clusterer import NLP
from .plot import plot_clusters, plot_clusters_hv  # noqa


def predict(clusterer, new_texts: List[str]) -> List[str]:
    nlp = NLP()
    test_labels, strengths = hdbscan.approximate_predict(
        clusterer, np.array([doc.vector for doc in nlp.pipe(new_texts)])
    )
    return test_labels, strengths


def soft_predict(clusterer, new_texts: List[str]) -> List[str]:
    nlp = NLP()
    test_labels = hdbscan.membership_vector(
        clusterer, np.array([doc.vector for doc in nlp.pipe(new_texts)])
    )
    return test_labels


def spacy_keys_only(cat: Catalog, min_cluster_size=2):
    nlp = NLP("fr_core_news_lg")
    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, prediction_data=True)

    keys = list(cat.aggregated.keys())
    vectors = []
    for doc in nlp.pipe(keys):
        vectors.append(doc.vector)
    vnp = np.array(vectors)
    clusterer.fit(vnp)
    labels = clusterer.labels_
    clusters = {
        cluster: [key for i, key in enumerate(keys) if labels[i] == cluster]
        for cluster in range(-1, labels.max())
    }
    return clusterer, clusters


def spacy_aggregated(cat: Catalog, min_cluster_size=2):
    nlp = NLP("fr_core_news_lg")
    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, prediction_data=True)

    keys = list(cat.aggregated.keys())
    content = [f"{key} : {', '.join(values)}" for key, values in cat.aggregated.items()]
    vectors = []
    for doc in nlp.pipe(content):
        vectors.append(doc.vector)
    vnp = np.array(vectors)
    clusterer.fit(vnp)
    labels = clusterer.labels_
    clusters = {
        cluster: [key for i, key in enumerate(keys) if labels[i] == cluster]
        for cluster in range(-1, labels.max())
    }
    return clusterer, clusters
