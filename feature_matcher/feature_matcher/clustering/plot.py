import random

import holoviews as hv
import hvplot.pandas  # noqa
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.manifold import TSNE


def plot_clusters_hv(
    clusterer,
    data,
    labels,
    show_like=None,
    show_clusters=False,
    mixed_points=None,
    close_points=None,
):
    new_values = TSNE().fit_transform(data)
    palette = sns.color_palette("Paired", clusterer.labels_.max() + 1)
    colors = [palette[x] if x >= 0 else (0.5, 0.5, 0.5) for x in clusterer.labels_]
    cluster_member_colors = [
        sns.desaturate(x, p) for x, p in zip(colors, clusterer.probabilities_)
    ]
    borders = [
        "red" if show_like and show_like.lower() in l.lower() else None for l in labels
    ]

    df = pd.DataFrame(
        {
            "TSNE dim 1": new_values.T[0],
            "TSNE dim 2": new_values.T[1],
            "key": labels,
            "cluster": clusterer.labels_,
            "color": cluster_member_colors,
            "proba": clusterer.probabilities_,
            "alpha": 0.1 + 0.9 * clusterer.probabilities_,
        }
    )
    plot = df.hvplot.scatter(
        x="TSNE dim 1",
        y="TSNE dim 2",
        by="cluster",
        alpha="alpha",
        line_color=borders,
        legend=None,
        height=800,
        width=800,
        hover_cols=["key", "cluster", "proba"],
    )
    if show_clusters:
        sample_labels = {}
        for i, cluster in enumerate(clusterer.labels_):
            if cluster in sample_labels:
                continue
            sample_labels[cluster] = i
        cluster_labels = hv.Labels(
            {
                ("x", "y"): [new_values[i] for i in sample_labels.values()],
                "text": [labels[i] for i in sample_labels.values()],
            },
            ["x", "y"],
            "text",
        )
        overlay = plot * cluster_labels

        overlay.opts(hv.opts.Labels(text_font_size="6pt", xoffset=0.08))
    else:
        overlay = plot

    if mixed_points is not None:
        scatter = hv.Scatter([new_values[k] for k in mixed_points])
        overlay = overlay * scatter.opts(
            color="k", marker="o", fill_color=None, line_width=1, size=10
        )

    if close_points is not None:
        scatter = hv.Scatter([new_values[k] for k in close_points])
        overlay = overlay * scatter.opts(
            color="r", marker="o", fill_color=None, line_width=1, size=7
        )

    return overlay


def plot_clusters(
    clusterer, data, labels, show_clusters=False, show_ratio=0.1, show_like=None
):
    new_values = TSNE().fit_transform(data)
    palette = sns.color_palette("Paired", clusterer.labels_.max() + 1)
    colors = [palette[x] if x >= 0 else (0.5, 0.5, 0.5) for x in clusterer.labels_]
    cluster_member_colors = [
        sns.desaturate(x, p) for x, p in zip(colors, clusterer.probabilities_)
    ]

    x = []
    y = []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])

    plt.figure(figsize=(16, 16))
    plt.scatter(*new_values.T, s=50, linewidth=0, c=cluster_member_colors, alpha=0.25)
    if show_clusters:
        for i in range(clusterer.labels_.max()):
            idx = clusterer.labels_.tolist().index(i)
            plt.annotate(
                labels[idx],
                xy=(x[idx], y[idx]),
                xytext=(5, 2),
                textcoords="offset points",
                ha="right",
                va="bottom",
                size=10,
            )
    else:
        for i in range(len(x)):
            if show_like:
                condition = show_like.lower() in labels[i].lower()
            else:
                condition = random.random() < show_ratio
            if condition:
                plt.annotate(
                    labels[i],
                    xy=(x[i], y[i]),
                    xytext=(5, 2),
                    textcoords="offset points",
                    ha="right",
                    va="bottom",
                    size=10,
                )
    plt.show()
