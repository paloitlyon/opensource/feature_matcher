from collections import defaultdict
from typing import List

import yaml

from feature_matcher.entities import Catalog, Feature, Product


class PreClusterer:
    def __init__(self, names: List[str], member_lists: List[List[str]]):
        self.clusters: List[int] = list(range(len(names)))
        self.names: List[str] = names
        self.member_lists: List[List[str]] = member_lists
        self.key_to_cluster = defaultdict(lambda: -1)
        for i, members in zip(self.clusters, member_lists):
            for member in members:
                self.key_to_cluster[member] = i

    def __repr__(self) -> str:
        return "\n".join(
            [
                f"{name}: {members}"
                for name, members in zip(self.names, self.member_lists)
            ]
        )

    def brief(self) -> str:
        return (
            f"Validated features: {len(self.names)} clusters, "
            f"{len(self.key_to_cluster)} validated features in total"
        )

    @classmethod
    def from_yml(cls, filename: str) -> "PreClusterer":
        with open(filename, "r") as f:
            clustering = yaml.load(f, Loader=yaml.CLoader)["clusters"]
        return cls(
            [cluster["name"] for cluster in clustering],
            [cluster["validated"] for cluster in clustering],
        )

    def fit_transform(self, features: List[Feature]) -> List[str]:
        return [self.key_to_cluster[feature.name] for feature in features]

    def augment_catalog(self, catalog: Catalog) -> Catalog:
        """Identify keys not present in the catalog, and add shallow features"""
        additional_features = {}
        catalog_keys = list(catalog.aggregated.keys())
        for name in self.key_to_cluster.keys():
            if name not in catalog_keys:
                additional_features[name] = ""
        product = Product(additional_features)
        return Catalog("augmented", catalog.products + [product])
