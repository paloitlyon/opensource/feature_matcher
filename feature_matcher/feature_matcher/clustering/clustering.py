import datetime
from collections import Counter, defaultdict
from typing import List

import numpy as np
import yaml

from feature_matcher.clustering.pre_clusterer import PreClusterer
from feature_matcher.entities import Feature


class Clustering:
    def __init__(
        self,
        features: List[Feature],
        labels: List[str],
        cluster_labels,
        soft_clusters,
        origins: List[str] = None,
        pre_clusterer: PreClusterer = None,
    ):
        self.features: List[Feature] = features
        self.labels: List[str] = labels
        self.hard_clusters = cluster_labels
        self.nb_clusters: int = cluster_labels.max()
        self.cluster_ids: List[int] = list(range(self.nb_clusters))
        self.origins: List[str] = origins if origins else ["default"] * len(labels)
        # Handle soft clusters
        soft_clusters = (
            soft_clusters
            if soft_clusters is not None
            else np.zeros(shape=(len(self.hard_clusters), self.nb_clusters))
        )
        self.soft_clusters = soft_clusters.argmax(axis=1)
        self.soft_clusters_probs = soft_clusters
        self.pre_clusterer = pre_clusterer
        self.semi_supervised = self.pre_clusterer is not None

    def __repr__(self) -> str:
        rep = ""
        width = max(len(w) for w in self.origins)
        for i in range(self.nb_clusters):
            members = np.where(self.hard_clusters == i)[0]
            rep += f"------- Cluster {i} -------\n"
            for k in members:
                rep += f"{self.origins[k]}{' ' * (width - len(self.origins[k]))} | {self.labels[k]}\n"

            potential = set(
                np.where(
                    (self.soft_clusters == i)
                    & (self.soft_clusters_probs.max(axis=1) > 0)
                )[0]
            ) - set(members)
            if potential:
                rep += "\n  ++++ possible candidates\n"
            for k in potential:
                rep += (
                    f"  {self.origins[k]}{' ' * (width - len(self.origins[k]))} | "
                    f"p={round(self.soft_clusters_probs[k].max(), 2):.2f} | {self.labels[k]}\n"
                )
            rep += "\n\n"
        return rep

    def fix_preclustered(self, log_clustering_spreads=False):
        """
        Identify keys that have been spread over several clusters,
        determine which cluster corresponds to the validated one
        and restore validated cluster integrity
        """

        def map_clusters():
            """Identify the correspondances between the validated clusters and the new ones"""
            clusters_matches = defaultdict(list)
            for feature, predicted_cluster in zip(self.features, self.hard_clusters):
                expected_cluster = self.pre_clusterer.key_to_cluster[feature.name]
                if expected_cluster == -1:
                    continue
                clusters_matches[expected_cluster].append(predicted_cluster)
            # Count numbers of occurence and set the mapping to the most represented cluster
            counters = {
                expected: Counter(predicted)
                for expected, predicted in clusters_matches.items()
            }
            cluster_mapping = {
                expected: max(counter, key=lambda x: counter[x])
                for expected, counter in counters.items()
            }
            return clusters_matches, cluster_mapping

        def has_validated_spread(clusters_matches):
            """Detects if a validated cluster has been spread over several custers in the result"""
            errors = False
            for expected, targets in clusters_matches.items():
                targets = set(targets)
                if len(targets) > 1:
                    errors = True
                    print(
                        f"Validated cluster '{self.pre_clusterer.names[expected]}' ({expected}) has been"
                        f" spread over {len(targets)} clusters {targets}"
                    )
            if not errors:
                print("No cluster spread ! \o/")
                return False
            return True

        def fix_clusters(cluster_mapping):
            """Set the clusters to the original cluster (in the new clustering reference)"""
            fixed_clusters = self.hard_clusters.copy("F")
            fixed_soft_clusters_probs = self.soft_clusters_probs.copy("F")
            for i, feature in enumerate(self.features):
                expected_cluster = self.pre_clusterer.key_to_cluster[feature.name]
                if expected_cluster == -1:
                    continue
                new_cluster = cluster_mapping[expected_cluster]
                fixed_clusters[i] = new_cluster
                fixed_soft_clusters_probs[i] = np.zeros_like(
                    self.soft_clusters_probs[i]
                )
                fixed_soft_clusters_probs[i][new_cluster] = 1
            return np.array(fixed_clusters), fixed_soft_clusters_probs

        if self.semi_supervised:
            clusters_matches, cluster_mapping = map_clusters()
            if log_clustering_spreads:
                has_validated_spread(clusters_matches)
            new_clusters, new_soft_clusters = fix_clusters(cluster_mapping)
            # Persist the mapping in the pre-clusterer
            self.pre_clusterer.cluster_mapping = cluster_mapping
            return Clustering(
                self.features,
                self.labels,
                new_clusters,
                new_soft_clusters,
                self.origins,
                self.pre_clusterer,
            )
        return self

    def get_cluster_keys(self):
        clusters = []
        for i in self.cluster_ids:
            clustered = np.where(self.hard_clusters == i)[0]
            potential = set(
                np.where(
                    (self.hard_clusters == -1)
                    & (self.soft_clusters == i)
                    & (self.soft_clusters_probs.max(axis=1) > 0)
                )[0]
            ) - set(clustered)
            clusters.append(
                {
                    "cluster_id": i,
                    "clustered": [self.features[k].name for k in clustered],
                    "potential": [self.features[k].name for k in potential],
                }
            )
        return clusters

    def tag_validated(self, cluster_output):
        """
        Mark pre-clustered as validated
        Note: clusters are considered alread "fixed", meaning that each validated
            feature is already in the correct cluster
        """
        tagged_clusters = []
        for cluster in cluster_output:
            validated = [
                feature
                for feature in cluster["clustered"] + cluster["potential"]
                if self.pre_clusterer.key_to_cluster[feature] != -1
            ]
            clustered = [
                feature
                for feature in cluster["clustered"]
                if self.pre_clusterer.key_to_cluster[feature] == -1
            ]
            potential = [
                feature
                for feature in cluster["potential"]
                if self.pre_clusterer.key_to_cluster[feature] == -1
            ]
            tagged_clusters.append(
                {
                    "cluster_id": cluster["cluster_id"],
                    "validated": validated,
                    "clustered": clustered,
                    "potential": potential,
                }
            )
        return tagged_clusters

    def name_clusters(self, cluster_output):
        named_clusters = []
        assert self.pre_clusterer.cluster_mapping is not None
        new_to_name = {
            v: self.pre_clusterer.names[k]
            for k, v in self.pre_clusterer.cluster_mapping.items()
        }
        for cluster in cluster_output:
            cluster_id = cluster["cluster_id"]
            name = new_to_name.get(
                cluster_id,
                f"cluster_{cluster_id}_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}",
            )
            named_clusters.append(
                {
                    "name": name,
                    "validated": cluster["validated"],
                    "clustered": cluster["clustered"],
                    "potential": cluster["potential"],
                }
            )
        return named_clusters

    def to_yml(self, filename, mode="keys"):
        if mode == "keys":
            clustering = self.get_cluster_keys()
        else:
            raise NotImplemented("only keys supported")
        if self.semi_supervised:
            clustering = self.tag_validated(clustering)
            clustering = self.name_clusters(clustering)

        if filename is None:
            return yaml.dump({"clusters": clustering}, allow_unicode=True)
        else:
            with open(filename, "w+") as f:
                yaml.dump({"clusters": clustering}, f, allow_unicode=True)
