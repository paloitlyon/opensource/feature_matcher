import json
from collections import defaultdict
from functools import total_ordering
from pathlib import Path
from typing import Dict, List, Tuple, Union

import pandas as pd

from feature_matcher import constants


def to_json(content_dict: dict, filename: Union[str, Path]) -> None:
    if isinstance(filename, str):
        filename = Path(filename)
    filename.parent.mkdir(exist_ok=True, parents=True)
    with open(filename, "w+") as f:
        json.dump(content_dict, f)


class Feature:
    def __init__(self, name, value) -> None:
        self.name = str(name)
        self.value = str(value)

    def __repr__(self) -> str:
        return f"{self.name}: {self.value}"

    def __eq__(self, other: object) -> bool:
        return (self.name == other.name) and (self.value == other.value)

    def __hash__(self) -> int:
        return (self.name + self.value).__hash__()

    def similarity(self, other: "Feature", sim_func=None, **kwargs) -> float:
        if sim_func is None:
            return 1 if self.name == other.name else 0
        return sim_func(self, other, **kwargs)


@total_ordering
class Match:
    def __init__(self, src: str, tgt: str, score: float) -> None:
        self.source: str = src
        self.target: str = tgt
        self.score: float = score

    def __repr__(self) -> str:
        return f"{self.source} --> {self.target} = {self.score:2f}"

    def __eq__(self, other: "Match") -> bool:
        return self.score == other.score

    def __lt__(self, other: "Match") -> bool:
        return self.score < other.score


class MatchList:
    def __init__(self, matches: List[Match]) -> None:
        self.matches = matches

    def __repr__(self) -> str:
        return "  " + "\n  ".join(str(match) for match in self.matches)

    def __add__(self, other: "MatchList") -> "MatchList":
        return MatchList(self.matches + other.matches)

    def __len__(self) -> int:
        return len(self.matches)

    def __getitem__(self, i) -> Match:
        return self.matches[i]

    def __iter__(self):
        yield from self.matches

    @classmethod
    def from_dict(cls, _dict: dict) -> "MatchList":
        return cls([Match(src, tgt, 1) for src, tgt in _dict.items()])

    def to_dict(self) -> Dict[str, Tuple[str, float]]:
        out = defaultdict(list)
        for m in self.matches:
            out[m.source].append((m.target, m.score))
        return out

    def to_json(self, filename: Union[str, Path]) -> None:
        to_json(self.to_dict(), filename)

    def get_best(self) -> Match:
        return max(self.matches)

    def remove_feature(self, feature, is_source=True) -> None:
        if is_source:
            self.matches = [match for match in self.matches if match.source != feature]
        else:
            self.matches = [match for match in self.matches if match.target != feature]

    def remove_from_match(self, match: Match) -> None:
        self.matches = [
            elem
            for elem in self.matches
            if (match.source != elem.source) and (match.target != elem.target)
        ]


class DuplicateEntryError(Exception):
    pass


class Matching(MatchList):
    def __init__(self, matches: List[Match]) -> None:
        super().__init__(matches)
        self.assert_correct()
        self.dict = {m.source: m for m in self.matches}

    def __repr__(self) -> str:
        if len(self.matches) == 0:
            return "{}"
        return (
            "{\n"
            + "".join([f"  {m}" for m in sorted(self.matches, key=lambda x: x.source)])
            + "\n}"
        )

    def assert_correct(self) -> None:
        sources = [m.source for m in self.matches]
        targets = [m.target for m in self.matches]
        if len(sources) > len(set(sources)):
            raise DuplicateEntryError(f"One of the source key is duplicated: {sources}")
        if len(sources) > len(set(sources)):
            raise DuplicateEntryError(f"One of the target key is duplicated: {targets}")

    def get_score(self, gt: "Matching", rating_func=None) -> float:
        """
        Compares a matching with the ground truth.
        The output is the ratio of correct matches of the ground truth matching
        found in the calling matching.
        """
        if rating_func is not None:
            return rating_func(self, gt)
        return sum(
            1
            for match in gt
            if match.source in self.dict
            and self.dict[match.source].target == match.target
        ) / len(gt)


class Product:
    def __init__(self, product_dict, _id=None, category=None) -> None:
        self.id = _id
        self.category = category
        self.features = {
            name: Feature(name, value) for name, value in product_dict.items()
        }

    def __getitem__(self, i) -> Feature:
        return self.features[i]

    def __repr__(self) -> str:
        return (
            "{"
            + "".join([f"\n  {feature}," for feature in self.features.values()])
            + "\n}"
        )

    @classmethod
    def from_json(cls, _json: str, _id=None, category=None) -> "Product":
        return cls(json.loads(_json), _id, category)

    def keys(self):
        return [f for f in self.features]

    def values(self):
        return [f.value for f in self.features.values()]

    def items(self):
        return [(f.name, f.value) for f in self.features.values()]

    def match_scores(
        self, other_feature: Feature, sim_func=None, **kwargs
    ) -> List[Match]:
        return [
            Match(
                feature.name,
                other_feature.name,
                feature.similarity(other_feature, sim_func=sim_func, **kwargs),
            )
            for feature in self.features.values()
        ]

    def matches(
        self, other: "Product", sim_func=None, match_func=None, **kwargs
    ) -> MatchList:
        """
        Returns a list of matches unfiltered
        Matches are from the calling Product to the other Product
        """
        sim_kwargs = kwargs.get("sim_kwargs", {})
        if match_func is not None:
            return match_func(self, other, sim_func=sim_func, **kwargs)
        # Default: get matches in decreasing score order
        return sum(
            [
                MatchList(self.match_scores(feature, sim_func=sim_func, **sim_kwargs))
                for feature in other.features.values()
            ],
            start=MatchList([]),
        )

    def match(
        self, other: "Product", sim_func=None, match_func=None, **kwargs
    ) -> Matching:
        """
        Returns a list of unique matches
        Matches are from the calling Product to the other Product
        """
        matches = self.matches(other, sim_func, match_func, **kwargs)
        result_matches = []
        while len(matches) > 0:
            best = matches.get_best()
            if best.score <= constants.EPSILON:
                break
            result_matches.append(best)
            matches.remove_from_match(best)
        return Matching(result_matches)


def CatalogMatching(MatchList):
    pass


class Catalog:
    def __init__(
        self, name, products: Union[List[dict], List[Product]], source: str = None
    ) -> None:
        if not products:
            self.products = []
        else:
            if isinstance(products[0], str):
                products = [Product(json.loads(prod), source) for prod in products]
            if not isinstance(products[0], Product):
                products = [Product(prod, source) for prod in products]
            self.products = products
        self.name = name
        self.aggregated = defaultdict(list)
        for product in self.products:
            for feature in product.features.values():
                self.aggregated[feature.name].append(feature.value)

    def __repr__(self) -> str:
        return (
            f"Catalog {self.name} : {len(self.products)} products, "
            f"{len(self.aggregated)} features, "
            f"{sum(len(vals) for vals in self.aggregated.values())} values"
        )

    def __add__(self, other: "Catalog") -> "Catalog":
        cat = Catalog(self.name, self.products)
        cat.products.extend(other.products)
        for product in other.products:
            for feature in product.features.values():
                self.aggregated[feature.name].append(feature.value)
        return cat

    def brief(self) -> str:
        return self.__repr__()

    @classmethod
    def from_csv(cls, filename: Union[str, Path], catalog_name: str) -> "Catalog":
        if isinstance(filename, Path):
            filename = str(filename)
        catalog_df = pd.read_csv(filename)
        catalog_col = (
            catalog_name
            if catalog_name.startswith("data_")
            else ("data_" + catalog_name)
        )
        assert (
            catalog_col in catalog_df.columns
        ), f"Name not found in columns: {catalog_col}"
        products = catalog_df[catalog_col].dropna().to_list()
        return cls(catalog_name, products, source=catalog_col)

    @classmethod
    def global_from_csv(
        cls, filename: Union[str, Path], category_ids: List[int] = None
    ) -> "Catalog":
        if isinstance(filename, Path):
            filename = str(filename)
        catalog_df = pd.read_csv(filename)
        if category_ids is not None:
            catalog_df = catalog_df[catalog_df["product_type_id"].isin(category_ids)]
        data_cols = [
            col
            for col in catalog_df.columns
            if isinstance(col, str)
            and col.startswith("data_")  # HYPOTHESIS: data columns start with "data_"
        ]
        return sum(
            [
                cls("global", catalog_df[col].dropna().to_list(), source=col)
                for col in data_cols
            ],
            start=cls("global", []),
        )

    def aggregated_product(self, aggregation_func=None, **kwargs) -> Product:
        if aggregation_func:
            return aggregation_func(self, **kwargs)
        return Product(
            {key: "\n".join(value_list) for key, value_list in self.aggregated.items()}
        )

    def get_features_and_labels(self, mode="first_value"):
        if mode == "first_value":
            features = [
                Feature(name, values[0]) for name, values in self.aggregated.items()
            ]
            labels = [f"{f.name} : {f.value}" for f in features]
        else:
            raise NotImplemented("Only first_value supported")
        return features, labels

    def matches(
        self, other: "Catalog", agg_func=None, sim_func=None, match_func=None, **kwargs
    ) -> CatalogMatching:
        sim_kwargs = kwargs.get("sim_kwargs", {})
        agg_kwargs = kwargs.get("agg_kwargs", {})
        self_prod = self.aggregated_product(aggregation_func=agg_func, **agg_kwargs)
        other_prod = other.aggregated_product(aggregation_func=agg_func, **agg_kwargs)
        if match_func is not None:
            return match_func(self_prod, other_prod, sim_func=sim_func, **kwargs)
        return sum(
            [
                MatchList(
                    self_prod.match_scores(feature, sim_func=sim_func, **sim_kwargs)
                )
                for feature in other_prod.features.values()
            ],
            start=MatchList([]),
        )

    def match(
        self,
        other: "Catalog",
        return_matches=False,
        agg_func=None,
        sim_func=None,
        match_func=None,
        **kwargs,
    ) -> Matching:
        """
        Returns a list of unique matches
        Matches are from the calling Product to the other Product
        """
        matches = self.matches(other, agg_func, sim_func, match_func, **kwargs)
        result_matches = []
        while len(matches) > 0:
            best = matches.get_best()
            if best.score <= constants.EPSILON:
                break
            result_matches.append(best)
            matches.remove_from_match(best)
        if return_matches:
            return Matching(result_matches), matches
        else:
            return Matching(result_matches)


class ProductSet:
    def __init__(self, products: List[Product]):
        self.products: List[Product] = products

    def add(self, product: Product):
        self.products.append(product)

    def __repr__(self) -> str:
        return "[" + ", ".join([str(prod) for prod in self.products]) + "]"

    @classmethod
    def from_csv(cls, filename: Union[str, Path], iloc: int = 0) -> "ProductSet":
        if isinstance(filename, Path):
            filename = str(filename)
        product = pd.read_csv(filename).iloc[iloc]
        data_cols = [
            col
            for col in product.index
            if isinstance(col, str)
            and col.startswith("data_")  # HYPOTHESIS: data columns start with "data_"
            and not pd.isna(product[col])  # exclude empty data columns
        ]
        product_data = [Product.from_json(product[col], col) for col in data_cols]
        return cls(product_data)

    def brief(self) -> str:
        return (
            "["
            + ", ".join([f"{prod.id}({len(prod.features)})" for prod in self.products])
            + "]"
        )
