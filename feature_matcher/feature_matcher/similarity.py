import spacy

from feature_matcher.entities import Feature

nlp = spacy.load("fr_core_news_lg")


def baseline(feature: Feature, other: Feature, **kwargs) -> float:
    """
    Uses the vector similarity of the names and values of each feature.
    Each name/value is consisdered as a whole and their vectors averaged before computing similarity.
    Therefore, the order of words in not accounted for, nor the proximity of each different word.
    """
    weight_name = kwargs.get("weight_name", 1)
    weight_value = kwargs.get("weight_value", 1)
    # Compute docs & cache them
    if weight_name != 0:
        if getattr(feature, "name_doc", None) is None:
            feature.name_doc = nlp(feature.name)
        if getattr(other, "name_doc", None) is None:
            other.name_doc = nlp(other.name)
    if weight_value != 0:
        if getattr(feature, "value_doc", None) is None:
            feature.value_doc = nlp(feature.value)
        if getattr(other, "value_doc", None) is None:
            other.value_doc = nlp(other.value)

    if kwargs.get("exact_takeover") and feature.name == other.name:
        return 1
    name_sim = feature.name_doc.similarity(other.name_doc) if weight_name > 0 else 0
    val_sim = feature.value_doc.similarity(other.value_doc) if weight_value > 0 else 0
    combined = (name_sim * weight_name + val_sim * weight_value) / (
        weight_name + weight_value
    )
    if combined < kwargs.get("threshold_zero", 0):
        return 0
    else:
        return combined
